# README #

For this to work:

Part 1. R and Rstudio installation (if not installed)

1. Install R-language from here: https://cran.r-project.org/
1. Install IDE - RStudio  - from here https://rstudio.com/products/rstudio/download/#download
1. Run RStudio. Look around the interface
On top-left is the code “Editor” you write code here; top -right – all loaded variables and tables
Bottom-left is the Console where code is executed; bottom-right – working directory and files

1. Copy and paste the code from snippet below into Editor; select it and press Ctrl-Enter to execute
Alternatively, one can install packages marked in yellow via GUI on the right-hand side
```{
    # take a set of libraries
    
    libs <- c('plyr', 'devtools', 'rstudioapi' ,'tidyverse','parallel' , 'reshape2', 'lubridate', 'scales' , 'rentrez') # a minimum set of libraries to init
    
    # this code will automatically install ones that're missing in your package list
    if (all(libs %in% installed.packages()[,"Package"])) {
    Sys.sleep(0.1)
     } else {
    install.packages(libs[!(libs %in% installed.packages()[,"Package"])])
    }
    # this code will load all libraries and will enable Rstudio to work with Bioconductor
    sapply(libs, FUN = function(x){require(x, character.only = T)})
    

```


Part 2. Installing Bioconductor and libraries  (if not installed)
``` {
    if (!requireNamespace("BiocManager", quietly = TRUE))
       install.packages("BiocManager")
    BiocManager::install(version = "3.10")

```

Bioconductor is a separate super-library which overlooks packages that work specifically for bioinformatics cases.
Bioinformatic libraries “BSgenome”, ‘msa’, ‘DECIPHER’ and others should be installed like:
```{
    BiocManager::install(‘BSgenome’) 
    BiocManager::install(‘msa’) 
    BiocManager::install(‘DECIPHER’) 

```

Part 3. Open rstudio and run the snippet below, or open 'create_sars_cov2_tree.R' directly and source it in the editor

```{
    source('create_sars_cov2_tree.R')

```

